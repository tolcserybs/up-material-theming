import * as React from 'react'

export interface ICounterState {
    value: number
}

type handleType = () => void

export class Counter extends React.Component<{}, ICounterState> {
    public state: ICounterState = { value: 0 }

    public render(): JSX.Element {
        return (
            <>
                <div>{this.state.value}</div>
                <button onClick={this.handleIncrement}>+</button>
                <button onClick={this.handleDecrement}>-</button>
            </>
        )
    }

    private handleDecrement: handleType = () => this.setState({ value: this.state.value - 1 })
    private handleIncrement: handleType = () => this.setState({ value: this.state.value + 1 })
}
