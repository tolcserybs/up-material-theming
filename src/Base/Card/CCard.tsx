import * as React from 'react'
import ICard from './ICard'
import BoxHeader from '../BoxHeader/CBoxHeader'
import Button from '../Button/CButton'
import { btnSize } from '../Button/IButton'
import styled from 'styled-components'

export class Card extends React.Component<ICard> {
    public static defaultProps = {
        title: 'Card Title',
        content: <div>default content</div>,
        button: {
            text: 'go',
            url: '#',
        },
    }

    public render(): React.ReactNode {
        const { title, content, button, theme, pageType } = this.props
        const CardWrapper = styled.div`
            ${theme.box.borderColor ? `border: 1px solid ${theme.box.borderColor};` : ''}
            width: ${theme.box.minWidth}px;
            overflow: hidden;
            border-radius: ${theme.box.borderRadius ? `${theme.box.borderRadius}px;` : ''};
            font-family: ${theme.font.name};
        `
        const ButtonWrapper = styled.div`
            display: flex;
            justify-content: ${theme.boxButton.justify};
            padding: ${theme.boxButton.padding.ver}px ${theme.boxButton.padding.hor}px;
        `

        const ContentWrapper = styled.div`
            padding: ${theme.boxContent.padding.ver}px ${theme.boxContent.padding.hor}px;
            font-family: ${theme.font.name};
            border-top: ${theme.boxContent.borderTop
                ? `${theme.boxContent.borderTop.width}px ${theme.boxContent.borderTop.style} ${
                      theme.boxContent.borderTop.color
                  }`
                : 'none'};
            border-bottom: ${theme.boxContent.borderBottom
                ? `${theme.boxContent.borderBottom.width}px ${
                      theme.boxContent.borderBottom.style
                  } ${theme.boxContent.borderBottom.color}`
                : 'none'};
        `

        return (
            <CardWrapper>
                <BoxHeader title={title} theme={theme.boxHeader} />
                <ContentWrapper>{content}</ContentWrapper>
                <ButtonWrapper>
                    <Button
                        url={button.url}
                        text={button.text}
                        theme={theme.button}
                        size={pageType === 'mobil' ? btnSize.SMALL : btnSize.MIDDLE}
                    />
                </ButtonWrapper>
            </CardWrapper>
        )
    }
}
