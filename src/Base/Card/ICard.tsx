import { ITheme } from '../../Themes/IThemesCollection'

interface IButtonInfo {
    text: string
    url: string
}

export default interface ICard {
    title: string
    content: any
    button: IButtonInfo
    theme: ITheme
    pageType?: string
}
