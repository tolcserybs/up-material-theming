import { buttonTheme } from '../../Themes/IThemesCollection'

export enum btnSize {
    SMALL = 'small',
    MIDDLE = 'middle',
    BIG = 'big',
}

export default interface IButton {
    text: string
    url: string
    theme: buttonTheme
    size: btnSize
}
