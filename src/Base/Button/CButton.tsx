import * as React from 'react'
import IButton, { btnSize } from './IButton'
import styled from 'styled-components'
import { buttonTheme, buttonSizes, backgroundColor } from '../../Themes/IThemesCollection'

const getSizeInfo = (themeSizes: buttonSizes, parameter: string, size: btnSize) =>
    themeSizes[size] ? themeSizes[size][parameter] : themeSizes.middle[parameter]

const getBackground = (backgroundInfo: backgroundColor) =>
    backgroundInfo.second
        ? backgroundInfo.main +
          ` linear-gradient(to bottom, rgba(${backgroundInfo.second},0) 0%,rgba(${
              backgroundInfo.second
          },1) 100%)`
        : backgroundInfo.main

const generateLinkButton = (theme: buttonTheme, size: btnSize) => styled.a`
    display: inline-block;
    color: ${theme.color.normal};
    background: ${getBackground(theme.background.normal)};
    padding: ${(function(sizeName: btnSize) {
        const paddingInfo = getSizeInfo(theme.size, 'padding', sizeName)
        return paddingInfo.hor + 'px ' + paddingInfo.ver + 'px'
    })(size)};
    ${() => {
        const borderRad: number | undefined = getSizeInfo(theme.size, 'borderRadius', size)
        return borderRad ? `border-radius: ${borderRad}px; ` : ''
    }}
    min-width: ${getSizeInfo(theme.size, 'minWidth', size)}px;
    height: ${getSizeInfo(theme.size, 'height', size)}px;
    line-height: ${getSizeInfo(theme.size, 'height', size)}px;
    font-size: ${getSizeInfo(theme.size, 'fontSize', size)}px;
    text-align: center;
    text-decoration: none;
    font-weight: bold; 
    ${
        theme.background.focus
            ? `
            :focus{
                background: ${getBackground(theme.background.focus)}
            }`
            : ''
    }
    ${
        theme.background.hover
            ? `
            :hover{
                background: ${getBackground(theme.background.hover)}
            }`
            : ''
    }    
`

export default class Button extends React.Component<IButton> {
    public render(): React.ReactNode {
        const LinkButton = generateLinkButton(this.props.theme, this.props.size)
        return <LinkButton href="{this.props.url}">{this.props.text}</LinkButton>
    }
}
