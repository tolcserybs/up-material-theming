import { boxHeaderTheme } from '../../Themes/IThemesCollection'

export enum boxTitleSize {
    SMALL = 'small',
    MIDDLE = 'middle',
    BIG = 'big',
}

export enum boxTitleAlign {
    LEFT = 'left',
    CENTER = 'center',
    RIGHT = 'right',
}

export interface IBoxHeaderStyle {
    backgroundColor: string
    titleStyle: {
        size: boxTitleSize
        align: boxTitleAlign
    }
}

export interface IBoxHeader {
    title: string
    theme: boxHeaderTheme
}
