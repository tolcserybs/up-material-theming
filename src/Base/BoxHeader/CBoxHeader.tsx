import * as React from 'react'
import { boxTitleSize, boxTitleAlign, IBoxHeader } from './IBoxHeader'
import styled from 'styled-components'

export default class BoxHeader extends React.Component<IBoxHeader> {
    public static defaultProps: IBoxHeader = {
        title: 'title',
    }
    public render(): React.ReactNode {
        const theme = this.props.theme
        const StyledBoxHeader = styled.h1`
            background-color: ${theme.backgroundColor};
            margin: ${theme.margin};
            ${() => {
                return theme.borderRadius
                    ? `border-radius: ${theme.borderRadius.join('px ')}px;`
                    : ''
            }}
            padding: ${`${theme.padding.hor}px ${theme.padding.ver}px`}
            text-align: ${theme.textAlign};
            color: ${theme.color};
            font-size: ${theme.fontSize}px;
            white-space: nowrap;
            overflow: hidden;
            height: ${theme.fontSize + theme.padding.ver * 2}px;
        `
        const { title } = this.props
        return <StyledBoxHeader>{title}</StyledBoxHeader>
    }
}
