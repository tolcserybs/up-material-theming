import { ITheme } from '../Themes/IThemesCollection'

export interface IFlightInfo {
    txt: string
    cityShort: string
    city: string
}

export interface IFlightCard {
    flightId: string
    fromInfo: IFlightInfo
    toInfo: IFlightInfo
    theme: ITheme
}
