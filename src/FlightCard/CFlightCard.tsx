import * as React from 'react'
import { Card } from '../Base/Card/CCard'
import { IFlightInfo, IFlightCard } from './IFlightCard'
import styled from 'styled-components'

class FInfoBox extends React.Component<IFlightInfo> {
    render() {
        const StyledInfoBox = styled.div`
            padding: 5px;
        `
        return (
            <StyledInfoBox>
                <div>
                    <div>{this.props.txt}</div>
                    <div>{this.props.cityShort}</div>
                    <div>{this.props.city}</div>
                </div>
            </StyledInfoBox>
        )
    }
}

const FCContent = styled.div`
    display: flex;
    justify-content: space-around;
`

export default class FlightCard extends React.Component<IFlightCard> {
    public render(): React.ReactNode {
        const cardContent = (
            <FCContent>
                <FInfoBox
                    txt={this.props.fromInfo.txt}
                    city={this.props.fromInfo.city}
                    cityShort={this.props.fromInfo.cityShort}
                />
                <div />
                <FInfoBox
                    txt={this.props.toInfo.txt}
                    city={this.props.toInfo.city}
                    cityShort={this.props.toInfo.cityShort}
                />
            </FCContent>
        )
        return (
            <Card
                title={`Flight ${this.props.flightId}`}
                content={cardContent}
                button={{
                    text: 'details',
                    url: '/flight',
                }}
                theme={this.props.theme}
            />
        )
    }
}
