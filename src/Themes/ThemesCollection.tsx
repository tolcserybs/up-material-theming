import { JustifyButton, IThemesCollection, borderStyles } from './IThemesCollection'

const Themes: IThemesCollection = {
    default: {
        font: {
            name: 'roboto',
            fileName: 'Roboto-Regular.ttf',
        },
        box: {
            borderColor: 'lightblue',
            minWidth: 300,
            borderRadius: 10,
        },
        boxHeader: {
            backgroundColor: 'lightblue',
            margin: 0,
            borderRadius: [10, 10, 0, 0],
            padding: {
                ver: 5,
                hor: 10,
            },
            textAlign: 'right',
            color: 'white',
            fontSize: 50,
        },
        boxContent: {
            padding: {
                ver: 10,
                hor: 10,
            },
            borderTop: {
                width: 4,
                color: '#7294cc',
                style: borderStyles.GROOVE,
            },
            borderBottom: {
                width: 1,
                color: 'rgba(159, 176, 204, 0.4)',
                style: borderStyles.GROOVE,
            },
        },
        boxButton: {
            padding: {
                hor: 5,
                ver: 5,
            },
            justify: JustifyButton.CENTER,
        },
        button: {
            size: {
                small: {
                    minWidth: 98,
                    height: 18,
                    fontSize: 12,
                    padding: {
                        ver: 2,
                        hor: 5,
                    },
                    borderRadius: 2,
                },
                middle: {
                    minWidth: 120,
                    height: 22,
                    fontSize: 16,
                    padding: {
                        ver: 5,
                        hor: 10,
                    },
                    borderRadius: 5,
                },
                big: {
                    minWidth: 200,
                    height: 30,
                    fontSize: 22,
                    padding: {
                        ver: 5,
                        hor: 10,
                    },
                    borderRadius: 8,
                },
            },
            color: {
                normal: '#FFF',
            },
            background: {
                normal: {
                    main: 'lightblue',
                    second: '32,124,202', //todo: converter function
                },
                hover: {
                    main: 'lightblue',
                    second: '64,156,234', //todo: converter function
                },
            },
        },
    },
    dark: {
        font: {
            name: 'oxygen',
            fileName: 'Oxigen-Regular.ttf',
        },
        box: {
            borderColor: 'rgba(0,0,0,0.7)',
            minWidth: 300,
            borderRadius: 10,
        },
        boxButton: {
            padding: {
                hor: 10,
                ver: 5,
            },
            justify: JustifyButton.RIGHT,
        },
        boxHeader: {
            backgroundColor: 'black',
            margin: 0,
            borderRadius: [10, 10, 0, 0],
            padding: {
                ver: 5,
                hor: 10,
            },
            textAlign: 'right',
            color: 'white',
            fontSize: 20,
        },
        boxContent: {
            padding: {
                ver: 10,
                hor: 10,
            },
            borderTop: {
                width: 10,
                color: '#a33',
                style: borderStyles.RIDGE,
            },
            borderBottom: {
                width: 1,
                color: '#a33',
                style: borderStyles.DOTTED,
            },
        },
        button: {
            size: {
                small: {
                    minWidth: 98,
                    height: 18,
                    fontSize: 12,
                    padding: {
                        ver: 2,
                        hor: 5,
                    },
                    borderRadius: 1,
                },
                middle: {
                    minWidth: 120,
                    height: 22,
                    fontSize: 16,
                    padding: {
                        ver: 5,
                        hor: 10,
                    },
                    borderRadius: 3,
                },
                big: {
                    minWidth: 200,
                    height: 30,
                    fontSize: 22,
                    padding: {
                        ver: 5,
                        hor: 10,
                    },
                    borderRadius: 5,
                },
            },
            color: {
                normal: '#FFF',
            },
            background: {
                normal: {
                    main: 'grey',
                    second: '0,0,0', //todo: converter function
                },
                hover: {
                    main: 'lightgrey',
                    second: '0,0,0', //todo: converter function
                },
            },
        },
    },
    simple: {
        font: {
            name: 'open',
            fileName: 'OpenSansCondensed-Light.ttf',
        },
        box: {
            borderColor: 'lightgrey',
            minWidth: 300,
        },
        boxHeader: {
            backgroundColor: 'darkGrey',
            margin: 0,
            padding: {
                ver: 5,
                hor: 10,
            },
            textAlign: 'left',
            color: 'black',
            fontSize: 50,
        },
        boxContent: {
            padding: {
                ver: 10,
                hor: 10,
            },
            borderTop: {
                width: 3,
                color: 'black',
                style: borderStyles.SOLID,
            },
        },
        boxButton: {
            padding: {
                hor: 5,
                ver: 20,
            },
            justify: JustifyButton.LEFT,
        },
        button: {
            size: {
                small: {
                    minWidth: 98,
                    height: 18,
                    fontSize: 12,
                    padding: {
                        ver: 2,
                        hor: 5,
                    },
                },
                middle: {
                    minWidth: 120,
                    height: 22,
                    fontSize: 16,
                    padding: {
                        ver: 5,
                        hor: 10,
                    },
                },
                big: {
                    minWidth: 200,
                    height: 30,
                    fontSize: 22,
                    padding: {
                        ver: 5,
                        hor: 10,
                    },
                },
            },
            color: {
                normal: '#FFF',
            },
            background: {
                normal: {
                    main: 'grey',
                },
                hover: {
                    main: '#555',
                },
            },
        },
    },
}

export default Themes
