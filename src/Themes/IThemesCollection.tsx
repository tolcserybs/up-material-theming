interface buttonPadding {
    ver: number
    hor: number
}

interface buttonSize {
    minWidth: number
    height: number
    fontSize: number
    padding: buttonPadding
    borderRadius?: number
}

export interface buttonSizes {
    small?: buttonSize
    middle: buttonSize
    big?: buttonSize
}

interface buttonFontColor {
    normal: string
    hover?: string
    focus?: string
}

export interface backgroundColor {
    //todo: move up
    main: string
    second?: string
}

interface backgroundColors {
    normal: backgroundColor
    hover?: backgroundColor
    focus?: backgroundColor
}

export interface buttonTheme {
    size: buttonSizes
    color: buttonFontColor
    background: backgroundColors
}

interface boxTheme {
    minWidth: number
    borderRadius?: number
    borderColor?: string
}

export interface boxHeaderTheme {
    backgroundColor: string
    margin: number
    borderRadius?: object
    padding: buttonPadding
    textAlign: string
    color: string
    fontSize: number
}

export enum JustifyButton {
    LEFT = 'flex-start',
    CENTER = 'center',
    RIGHT = 'flex-end',
}

interface boxButtonTheme {
    padding: buttonPadding
    justify: JustifyButton
}

export enum borderStyles {
    NONE = 'none',
    HIDDEN = 'hidden',
    DOTTED = 'dotted',
    DASHED = 'dashed',
    SOLID = 'solid',
    DOUBLE = 'double',
    GROOVE = 'groove',
    RIDGE = 'ridge',
    INSET = 'inset',
    OUTSET = 'outset',
    INITIAL = 'initial',
    INHERIT = 'inherit',
}

interface border {
    color: string
    width: number
    style: borderStyles
}

interface boxContentTheme {
    padding: buttonPadding
    borderTop?: border
    borderBottom?: border
}

interface fontInfo {
    fileName: string
    name: string
}

export interface ITheme {
    box: boxTheme
    boxHeader: boxHeaderTheme
    boxContent: boxContentTheme
    boxButton: boxButtonTheme
    button: buttonTheme
    font: fontInfo
}

export interface IThemesCollection {
    [key: string]: ITheme
}
