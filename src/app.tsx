import * as React from 'react'

import { Counter } from './counter'
import { Hello } from './hello'

type appType = () => React.ReactNode

// tslint:disable-next-line:variable-name
export const App: appType = (): React.ReactNode => (
    <div>
        <Hello name={'react-webpack-typescript-babel'} />
        <Counter />
    </div>
)
