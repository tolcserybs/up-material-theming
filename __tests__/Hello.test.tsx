import * as React from 'react'
import { Hello } from '../src/hello'
import renderer from 'react-test-renderer'
import { render } from 'enzyme'

it('component should exist', () => {
    expect(Hello).toBeDefined()
})

it('check if matches snapshot', () => {
    const wrapper = renderer.create(<Hello name="This is jest testing" />)
    expect(wrapper).toMatchSnapshot()
})

it('check if it contains given parameter', () => {
    const wrapper = render(<Hello name="This is jest testing" />)
    expect(wrapper.text()).toBe('This is jest testing')
})
