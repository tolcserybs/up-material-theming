import { App } from '../src/app'
import * as React from 'react'
import renderer from 'react-test-renderer'

it('component should exist', () => {
    expect(App).toBeDefined()
})

it('should render properly', () => {
    const wrapper = renderer.create(<App />)
    expect(wrapper).toMatchSnapshot()
})
