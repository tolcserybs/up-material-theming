import * as React from 'react'
import { storiesOf } from '@storybook/react'
import { text, select } from '@storybook/addon-knobs'
import { Card } from '../src/Base/Card/CCard'
import Themes from '../src/Themes/ThemesCollection'
import { createGlobalStyle } from 'styled-components'

const GlobalStyle = createGlobalStyle`
@font-face {
    font-family: roboto;
    src: url(${require(`../src/fonts/Roboto-Regular.ttf`)}); 
    format('truetype');
    font-weight: normal;
    font-style: normal;
}
@font-face {
    font-family: open;
    src: url(${require(`../src/fonts/OpenSansCondensed-Light.ttf`)});
    format('truetype');
    font-weight: normal;
    font-style: normal;
}
@font-face {
    font-family: oxygen;
    src: url(${require(`../src/fonts/Oxygen-Regular.ttf`)});
    format('truetype');
    font-weight: normal;
    font-style: normal;
}
`

const newTitle: string = 'story title'
const ThemeIDs = Object.keys(Themes)

storiesOf('base card', module).add('card', () => (
    <div>
        <GlobalStyle />
        <Card
            title={text('title', newTitle)}
            theme={Themes[select('theme', ThemeIDs, 'default')]}
            pageType={select(
                'page type',
                {
                    mobil: 'mobil',
                    desktop: 'desktop',
                },
                'desktop'
            )}
        />
    </div>
))
