import * as React from 'react'
import { storiesOf } from '@storybook/react'
import FlightCard from '../src/FlightCard/CFlightCard'
import { text, select } from '@storybook/addon-knobs'
import { createGlobalStyle } from 'styled-components'
import Themes from '../src/Themes/ThemesCollection'

const GlobalStyle = createGlobalStyle`
@font-face {
    font-family: roboto;
    src: url(${require(`../src/fonts/Roboto-Regular.ttf`)}); 
    format('truetype');
    font-weight: normal;
    font-style: normal;
}
@font-face {
    font-family: open;
    src: url(${require(`../src/fonts/OpenSansCondensed-Light.ttf`)});
    format('truetype');
    font-weight: normal;
    font-style: normal;
}
@font-face {
    font-family: oxygen;
    src: url(${require(`../src/fonts/Oxygen-Regular.ttf`)});
    format('truetype');
    font-weight: normal;
    font-style: normal;
}
`
const ThemeIDs = Object.keys(Themes)

storiesOf('flight card', module).add('flight card', () => (
    <div>
        <GlobalStyle />
        <FlightCard
            theme={Themes[select('theme', ThemeIDs, 'default')]}
            flightId={text('Flight ID', '2355')}
            fromInfo={{
                txt: 'from',
                city: 'Budapest',
                cityShort: 'BUD',
            }}
            toInfo={{
                txt: 'to',
                city: 'Debrecen',
                cityShort: 'DEB',
            }}
        />
    </div>
))
